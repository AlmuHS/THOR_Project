import lxml.etree as ET
import pandas as pd

import consulta_datos as CD
import consulta_xsd as XSD
import busca_fichero
import procesa_accion as pr_accion
import genera_log as gen_log
import maneja_csv as m_csv


class ProcesaXML:
    xml_tree: ET.Element
    fichero_xml: str
    fichero_conf: str

    def __init__(self, fichero_xml: str):
        self.fichero_xml = fichero_xml

    '''
	Genera un fichero de configuración en formato csv, a partir de un fichero XML
	El fichero incluye el nombre de sus campos, algunas observaciones sobre los mismos,
	además del campo que referencia cada uno (donde consultar su descripcion), y el fichero donde buscarlo
	'''

    def generar_plantilla_conf_xml(self, fichero_config: str = None):

        # inicializa buscador de nombres de fichero
        busca_fich_obj = busca_fichero.BuscaNombreFichero()

        csv = m_csv.ManejaCSV(fichero_config)

        if not fichero_config:
            fichero_config = busca_fich_obj.obtener_nombre_fichero_conf(
                self.fichero_xml)

        # crea las columnas del fichero
        csv.crea_df_columnas(["Campo", "Observaciones", "Describible",
                              "Campo que referencia", "Campo descriptor",
                              "Fichero que referencia", "Acción"])

        # obten el nombre del fichero XSD asociado al XML
        fichero_xsd = busca_fich_obj.obtener_fichero_xsd_xml(self.fichero_xml)

        # obten la ruta del fichero XSD asociado al XML
        ruta_xsd = busca_fich_obj.obtener_ruta_fichero_xsd(fichero_xsd)

        # obten la lista de campos del fichero y sus atributos a partir de su fichero XSD
        consulta_xsd = XSD.ConsultaXSD(ruta_xsd)
        dict_campos = consulta_xsd.obten_lista_campos()

        # obten la lista de campos para borrar
        lista_campos_borrar = []
        with open("campos_para_borrar.txt", "r") as fichero_campos_borrar:
            lista_campos_borrar = fichero_campos_borrar.read()

        # rellena las filas de configuración de cada campo, a partir de los atributos de la lista de campos
        for i, campo in enumerate(dict_campos):

            # obten la lista de atributos del campo
            atributos_campo = dict_campos[campo]

            # nombre del campo
            nombre_campo = atributos_campo["Campo"]

            # rellena las configuraciones y datos básicos, usando los atributos de ese campo
            csv.asigna_celda(i, "Campo", nombre_campo)
            csv.asigna_celda(i, "Observaciones",
                             atributos_campo["Observaciones"])

            csv.asigna_celda(i, "Describible", atributos_campo["Describible"])

            # Si el campo está en la lista de campos a borrar, marcalo para borrar
            if nombre_campo in lista_campos_borrar:
                csv.asigna_celda(i, "Acción", "borrar")
            elif atributos_campo["Descrito"] == "si":
                '''
                si el campo esta descrito en el fichero XSD, rellena los campos con los datos del XSD
                y marcalo como campo a describir
                '''
                csv.asigna_celda(i, "Acción", "describir")
                csv.asigna_celda(i, "Fichero que referencia", fichero_xsd)
                csv.asigna_celda(i, "Campo que referencia", nombre_campo)
                csv.asigna_celda(i, "Campo descriptor", nombre_campo)
            else:
                '''
                Si el campo no está descrito en el XSD, ni está en la lista para borrar, marcarlo para mantenerlo
                (El usuario podrá cambiar este valor en el fichero mas adelante)
                '''
                csv.asigna_celda(i, "Acción", "mantener")

        # exporta el dataframe a csv
        csv.exporta_csv()
    '''
    Devuelve diccionario de objetos con las posibles acciones de procesado a ejecutar
    '''

    def __obtener_acciones(self):
        ejecuta_accion = {}
        ejecuta_accion["borrar"] = pr_accion.ProcesaBorrar()
        ejecuta_accion["describir"] = pr_accion.ProcesaDescribir()
        ejecuta_accion["reemplazar"] = pr_accion.ProcesaReemplazar()
        ejecuta_accion["redondear"] = pr_accion.ProcesaRedondear()

        return ejecuta_accion

    '''
    Devuelve diccionario de objetos con las opciones de errores para añadir al log
    '''

    def __obtener_errores(self, logger: gen_log.ManejaLogger):
        anade_error = {}
        anade_error[pr_accion.Resultado.ERROR_NO_DESCRIPCION] = gen_log.GeneraErrorNoDesc(
            logger)

        anade_error[pr_accion.Resultado.ERROR_NO_FICHERO] = gen_log.GeneraErrorNoFichero(
            logger)

        anade_error[pr_accion.Resultado.ERROR_NO_NUMERO] = gen_log.GeneraErrorNoNum(
            logger)

        anade_error[pr_accion.Resultado.ERROR_NO_ANIO] = gen_log.GeneraErrorNoAnio(
            logger)

        anade_error[pr_accion.Resultado.ERROR_NO_ACCION] = gen_log.GeneraErrorNoAccion(
            logger)

        return anade_error

    '''
	Desnormaliza un fichero XML, añadiendo campos de descripcion para cada campo de código, y borrando los campos indicados.
	Recibe por parámetro el nombre del fichero XML a procesar, junto con un diccionario indicando
	la lista de acciones a realizar con cada uno de sus campos.

	En caso de no pasar un diccionario con las acciones, este será generado automáticamente a
	partir de la lista de campos conocidos, describiendo todos los campos que pertenezcan a esta lista

	Genera un nuevo fichero, con el nombre del original y sufijo _desc, el cual tendrá las modificaciones
	indicadas en el diccionario.
	'''

    def desnormalizar_xml(self, fichero_config: str, fichero_salida: str):

        # Abre el fichero XML como ElementTree, y obten su raíz para poder recorrerlo
        self.xml_tree = ET.parse(self.fichero_xml)
        raiz = self.xml_tree.getroot()

        # Crea un dataframe para recorrer el fichero de configuración
        csv_conf = m_csv.ManejaCSV(fichero_config)
        csv_conf.carga_fichero()

        # obten el numero de registros a procesar
        num_registros = len(list(raiz))

        # obten diccionario de objetos con las diferentes acciones de procesado
        ejecuta_accion = self.__obtener_acciones()

        # Inicializa el logger para el fichero XML a procesar
        logger = gen_log.ManejaLogger(self.fichero_xml)

        # Obten el diccionario de objetos para añadir los errores
        errores = self.__obtener_errores(logger)

        # inicializamos la variable exito. Si el valor final es inferior a 0, indicará que ha habido errores
        exito = 0

        # Recorre el fichero registro a registro
        for i, registro in enumerate(raiz):
            # por cada registro, recorre sus campos conocidos e intenta describirlos
            for campo_reg in registro:
                # nombre del campo
                nombre_campo = campo_reg.tag

                # encuentra la fila de este campo en el fichero de configuración
                fila = csv_conf.encuentra_fila_por_columna(
                    "Campo", nombre_campo)

                # buscar en el fichero de configuración la accion a realizar en dicho campo
                accion = csv_conf.lee_celda_fila(fila, "Acción")

                if not accion:
                    errores[pr_accion.Resultado.ERROR_NO_ACCION].anade_error_log(
                        campo_reg, i)
                elif accion != "mantener":
                    # invoca al método correspondiente según la accion a realizar
                    resultado = ejecuta_accion[accion].procesa(
                        registro, fila, campo_reg, self.fichero_xml)

                    if resultado != pr_accion.Resultado.CORRECTO:
                        # si hay algún error, añadelo al log
                        errores[resultado].anade_error_log(campo_reg, i)

                        # por cada error al procesar el fichero, se resta una unidad
                        exito = exito - 1
            print(
                f"Fichero {self.fichero_xml}: Procesado registro {i+1} de {num_registros}")

        # indenta el XML usando tabulaciones
        ET.indent(raiz, space="\t")

        # Guarda el XML modificado en el fichero de salida
        self.xml_tree.write(fichero_salida, encoding='utf-8')

        # exporta el log con los errores del procesado a un fichero
        logger.exportar_log()

        return exito


if __name__ == "__main__":
    # fichero_entrada = "datos/U04918AC0305_02.XML"
    fichero_entrada = "datos/U04918RH0101_04.XML"
    # fichero_entrada = "datos/U04918RH0201_01.XML"
    # fichero_entrada = "datos/U04918RH0301_05.XML"
    # fichero_entrada = "datos/U04918AX0204_02.XML"
    # fichero_entrada = "datos/U04918AX0203_02.XML"
    # fichero_entrada = "datos/U04918AC0301_06.XML"
    # fichero_entrada = "datos/U04918AC0101_03.XML"
    # fichero_entrada = "datos/U04918AX0202_04.XML"
    # fichero_entrada = "datos/U04918AX0201_03.XML"
    # fichero_entrada = "datos/U04918AX0103_01.XML"
    # fichero_entrada = "datos/U04918AX0102_02.XML"
    # fichero_entrada = "datos/U04918AX0101_02.XML"
    # fichero_entrada = "datos/U04918BU0101_09.XML"
    # fichero_entrada = "datos/U04918BU0201_01.XML"
    # fichero_entrada = "datos/U04918BU0202_02.XML"
    # fichero_entrada = "datos/U04918BU0102_02.XML"

    procesa = ProcesaXML(fichero_entrada)
    procesa.generar_plantilla_conf_xml()
    # procesa.desnormalizar_xml(fichero_entrada)
