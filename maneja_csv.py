import pandas as pd


class ManejaCSV:
    fichero_csv: str
    df_csv: pd.DataFrame

    def __init__(self, fichero_csv: str):
        self.fichero_csv = fichero_csv

    def carga_fichero(self):
        self.df_csv = pd.read_csv(self.fichero_csv, sep=";",
                                  dtype=str, keep_default_na=False)

    def crea_df_columnas(self, lista_columnas: list):
        self.df_csv = pd.DataFrame(columns=lista_columnas)

    def asigna_celda(self, num_fila: int, nombre_columna: str, valor: str):
        self.df_csv.at[num_fila, nombre_columna] = valor

    def obtener_num_filas(self):
        return len(self.df_csv)

    def obtener_fila_por_indice(self, num_fila: int):
        return self.df_csv.iloc[num_fila]

    def obtener_todas_filas(self):
        return list(self.df_csv)

    def lee_celda_fila(self, fila: pd.DataFrame, nombre_columna: str):
        return fila[nombre_columna].values[0]

    def lee_celda(self, num_fila: int, nombre_columna: str):
        return self.df_csv.at[num_fila, nombre_columna]

    def anadir_fila(self, dict_campos: dict):
        df_fila = pd.DataFrame(dict_campos)
        self.df_csv = self.df_csv.append(df_fila, ignore_index=True)

    def encuentra_fila_por_columna(self, nom_columna: str, valor_columna: str):
        fila = self.df_csv[self.df_csv[nom_columna] == valor_columna]
        return fila

    def cruza_columnas_fila(self, col_busq: str, valor_busq: str, col_valor: str):
        fila = self.encuentra_fila_por_columna(col_busq, valor_busq)
        valor = fila[col_valor].values[0]

        return valor

    def exporta_csv(self):
        self.df_csv.to_csv(self.fichero_csv, sep=";", index=False)
