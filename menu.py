from procesa_dataset import ProcesaDataset, OpcionProc
from genera_tablas import GeneraTablas
from busca_fichero import BuscaFichero


'''
Comprueba si el nombre del fichero es válido: comprueba si la extensión se corresponde con la solicitada
y si el fichero existe en el sistema
'''


def fichero_valido(nombre_fichero: str, ext_valida: str):
    busca_fich = BuscaFichero()

    fich_valido = False

    # Si el nombre de fichero contiene la extension admitida
    if ext_valida in nombre_fichero:
        # comprueba que el fichero exista en el sistema
        fich_valido = busca_fich.comprueba_fich_existe(nombre_fichero)

    return fich_valido


def pide_fichero(mensaje: str, extension: str):
    nombre_plantilla = input(mensaje)

    fich_valido = fichero_valido(nombre_plantilla, extension)

    while (not fich_valido):
        print("Error: fichero no existente o extensión no válida")
        nombre_plantilla = input(mensaje)

        fich_valido = fichero_valido(nombre_plantilla, extension)

    return nombre_plantilla


class OpcionMenu:
    def obtener_nombre(self):
        pass

    def ejecutar(self):
        pass


class OpcionGenCSVDesdeExcel(OpcionMenu):
    def obtener_nombre(self):
        return "Generar ficheros CSV desde Excel"

    def ejecutar(self):
        fichero_excel = pide_fichero(
            "Introduce la ruta del fichero XLS: ", ".xls")

        genera_tablas = GeneraTablas()
        genera_tablas.generar_csv_desde_xls(fichero_excel)


class OpcionGenPlantillaDataSet(OpcionMenu):
    def obtener_nombre(self):
        return "Generar plantilla de dataset"

    def ejecutar(self):
        nombre_plantilla = input(
            "Introduce nombre de la plantilla a generar: ")

        opcion = input("¿Buscar todos los ficheros? (s/n)")

        procesa_ds = ProcesaDataset()

        if opcion == "s":
            patron_fichero = "*"

            procesa_ds.genera_plantilla_dataset(nombre_plantilla, "*")
        else:
            patron_fichero = input("Introduce el patrón del nombre a buscar: ")

        procesa_ds.genera_plantilla_dataset(nombre_plantilla, patron_fichero)


class OpcionGenPlantillaConf(OpcionMenu):
    def obtener_nombre(self):
        return "Generar plantillas de configuración desde dataset"

    def ejecutar(self):
        nombre_plantilla = pide_fichero(
            "Introduce el nombre de la plantilla del dataset: ", ".csv")

        procesa_ds = ProcesaDataset()
        procesa_ds.generar_plantillas_conf_desde_lista(nombre_plantilla)


class OpcionProcesaDataset(OpcionMenu):
    def obtener_nombre(self):
        return "Procesar dataset"

    def ejecutar(self):
        nombre_plantilla = pide_fichero(
            "Introduce el nombre de la plantilla del dataset: ", ".csv")

        procesar_todo = input("¿Desea procesar todos los ficheros? (s/n): ")

        procesa_ds = ProcesaDataset()

        if procesar_todo == "s":
            procesa_ds.procesar_lista_xml(
                nombre_plantilla, OpcionProc.TOTAL.value)
        else:
            procesa_ds.procesar_lista_xml(
                nombre_plantilla, OpcionProc.CON_ERROR.value)


class OpcionCambiarVarConf(OpcionMenu):
    def ejecutar(self):
        None


opcion_ejecutar = {}
opcion_ejecutar[0] = OpcionGenCSVDesdeExcel()
opcion_ejecutar[1] = OpcionGenPlantillaDataSet()
opcion_ejecutar[2] = OpcionGenPlantillaConf()
opcion_ejecutar[3] = OpcionProcesaDataset()


def menu():

    # Muestra las opciones del menú por pantalla
    for num_opcion in opcion_ejecutar.keys():
        print(f"{num_opcion}. {opcion_ejecutar[num_opcion].obtener_nombre()}")

    eleccion = input("Elija opción: ")
    # Pregunta la opción a elegir hasta obtener una opción válida
    while (not eleccion.isnumeric()) or (int(eleccion) not in range(0, len(opcion_ejecutar))):
        eleccion = input("Elija opción: ")

    # Ejecuta la opción seleccionada
    eleccion_num = int(eleccion)
    opcion_ejecutar[eleccion_num].ejecutar()


if __name__ == "__main__":
    menu()
