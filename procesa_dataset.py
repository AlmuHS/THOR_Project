import pandas as pd
import enum

import procesa_xml as proc_xml
import maneja_csv as man_csv
import busca_fichero as busca_fich
import variables_conf as var_conf


class OpcionProc(enum.Enum):
    TOTAL = 1
    SIN_ERROR = 2
    CON_ERROR = 3


class EstadoFichero(enum.Enum):
    NO_PROCESADO = 1
    FORMATO_CORRECTO = 2
    FORMATO_NO_CORRECTO = 3
    PROCESADO_CON_DESCRIPCIONES = 4
    PROCESADO_CON_ERRORES = 5
    ERRORES_INTEGRIDAD_REFERENCIAL = 6
    ERRORES_SEMANTICA_SINTAXIS = 7
    INSERTADO_EN_CKAN = 8


class ProcesaDataset:

    def _anade_fichero_dataset(self, fichero_entrada, fichero_salida, fichero_config):
        None
    '''
    Genera una tabla con los ficheros a procesar, y sus opciones de configuración:
    La lista de ficheros a procesar se obtiene automáticamente, haciendo una búsqueda en
    el directorio indicado, según el patrón indicado

    Recibe los siguientes parámetros:
    fichero_plantilla: nombre del fichero donde crearemos la plantilla
    directorio_ficheros: directorio donde vamos a buscar los ficheros
    patron_nombre: patrón para buscar los ficheros, en sintaxis tipo bash
    '''

    def genera_plantilla_dataset(self, nombre_plantilla: str, patron_nombre: str):

        # encuentra los ficheros a procesar
        dir_ficheros = var_conf.RUTA_FICH_PROCESAR
        busca_ficheros = busca_fich.BuscaFichero()
        csv = man_csv.ManejaCSV(nombre_plantilla)

        # anade la extensión XML al patrón del nombre del fichero
        patron_con_ext = f"{patron_nombre}.XML"

        # busca ficheros que coincidan con dicho patrón en el directorio indicado
        lista_ficheros = busca_ficheros.busca_fich_patron(
            dir_ficheros, patron_con_ext)

        # crea las columnas del fichero
        lista_columnas = ["Dataset", "Fichero_entrada", "Fichero_salida",
                          "Fichero_config", "Estado", "Error", "Fecha_procesado"]

        csv.crea_df_columnas(lista_columnas)

        # inicializa un objeto para buscar los nombres de los ficheros necesarios
        busca_nom_fich = busca_fich.BuscaNombreFichero()

        # por cada fichero XML, añade una fila rellenando el campo "Fichero_entrada" con su nombre
        for i, fichero in enumerate(lista_ficheros):
            # elimina la ruta en el fichero XML
            fichero_entrada = busca_nom_fich.obtener_fichero_sin_ruta(fichero)

            # busca el nombre de su dataset (será el nombre de su fichero XSD sin extensión)
            dataset = busca_nom_fich.obtener_dataset_xml(fichero)

            # Rellena los campos correspondientes
            csv.asigna_celda(i, "Dataset", dataset)
            csv.asigna_celda(i, "Fichero_entrada", fichero_entrada)

            # El estado por defecto será "No procesado"
            csv.asigna_celda(i, "Estado", EstadoFichero.NO_PROCESADO.value)

            # Añade el nombre del fichero que contendrá los datos del XML ya procesado
            fichero_salida = busca_nom_fich.obtener_nombre_fichero_procesado(
                fichero_entrada)
            csv.asigna_celda(i, "Fichero_salida", fichero_salida)

            # Añade el nombre del fichero de configuración asociado al XML
            # (Será el mismo nombre que el XML añadiendo el sufijo "_conf")
            fich_config = busca_nom_fich.obtener_nombre_fichero_conf(
                fichero_entrada)
            csv.asigna_celda(i, "Fichero_config", fich_config)

        # exporta el dataframe a csv
        csv.exporta_csv()

    '''
    Genera una colección de plantillas de ficheros de configuración,
    a partir de una lista de ficheros XML (un fichero por línea, incluyendo ruta)
    '''

    def generar_plantillas_conf_desde_lista(self, fichero_lista: str):

        csv = man_csv.ManejaCSV(fichero_lista)
        csv.carga_fichero()

        busca_nom_fich = busca_fich.BuscaNombreFichero()

        for i in range(csv.obtener_num_filas()):
            nombre_fichero = csv.lee_celda(i, "Fichero_entrada")
            fich_config = csv.lee_celda(i, "Fichero_config")

            ruta_fichero = busca_nom_fich.obtener_ruta_fichero_procesado(
                nombre_fichero)
            procesa = proc_xml.ProcesaXML(ruta_fichero)

            if not fich_config:
                procesa.generar_plantilla_conf_xml()
            else:
                ruta_config = busca_nom_fich.obtener_ruta_fichero_conf(
                    fich_config)
                procesa.generar_plantilla_conf_xml(ruta_config)

            print(f"generada plantilla para fichero {nombre_fichero}")
    '''
    Indica si un fichero es apto para su procesamiento,
    según el estado indicado en la configuración del dataset
    y la opción de procesamiento elegida por el usuario
    '''

    def _fichero_apto(self, opcion: int, estado: int):
        return (estado == EstadoFichero.NO_PROCESADO) or \
            (opcion == OpcionProc.TOTAL.value) or \
            (opcion == OpcionProc.CON_ERROR.value and estado ==
             EstadoFichero.PROCESADO_CON_ERRORES.value)

    '''
    Procesa una lista de ficheros XML, ejecutando un desnormalizado en cada uno de ellos,
    a partir de un fichero de configuración con las acciones de cada campo

    Recibe por parámetro el nombre del fichero que contiene la lista de XML,
    junto a una opción indicando si es necesario generar un fichero de configuración
    '''

    def procesar_lista_xml(self, fichero_lista: str, opcion: int):

        csv = man_csv.ManejaCSV(fichero_lista)
        csv.carga_fichero()
        busca_nom_fich = busca_fich.BuscaNombreFichero()
        busca_fichero = busca_fich.BuscaFichero()

        for i in range(csv.obtener_num_filas()):

            # Obten la ruta del fichero a procesar
            nombre_fichero = csv.lee_celda(i, "Fichero_entrada")
            ruta_fichero = busca_nom_fich.obtener_ruta_fichero_procesar(
                nombre_fichero)

            fich_config = csv.lee_celda(i, "Fichero_config")
            ruta_config = busca_nom_fich.obtener_ruta_fichero_conf(fich_config)

            # Comprueba si el fichero a procesar y su configuración existen en la ruta indicada
            config_existe = busca_fichero.comprueba_fich_existe(ruta_config)
            fich_existe = busca_fichero.comprueba_fich_existe(ruta_fichero)

            # Lee el estado actual del fichero
            estado = int(csv.lee_celda(i, "Estado"))

            # Si el fichero o su configuración no existen, añade un error
            if not config_existe:
                csv.asigna_celda(
                    i, "Error", f"Fichero {ruta_config} no encontrado")

                # Cambia su estado original por el de "no procesado"
                estado = EstadoFichero.NO_PROCESADO.value

            elif not fich_existe:
                csv.asigna_celda(
                    i, "Error", f"Fichero {ruta_fichero} no encontrado")

                # Cambia su estado original por el de "no procesado"
                estado = EstadoFichero.NO_PROCESADO.value

            # Si el fichero es apto, inicia el procesado
            elif self._fichero_apto(opcion, estado):
                # Obten la ruta del fichero de salida
                fich_salida = csv.lee_celda(i, "Fichero_salida")
                ruta_salida = busca_nom_fich.obtener_ruta_fichero_procesado(
                    fich_salida)

                # Inicia el procesamiento del fichero
                procesa = proc_xml.ProcesaXML(ruta_fichero)
                print(f"Procesando fichero {nombre_fichero}\n")
                exito = procesa.desnormalizar_xml(ruta_config, ruta_salida)
                print(f"Procesado fichero {nombre_fichero}\n")

                if exito == 0:
                    estado = EstadoFichero.PROCESADO_CON_DESCRIPCIONES.value
                else:
                    estado = EstadoFichero.PROCESADO_CON_ERRORES.value

            csv.asigna_celda(i, "Estado", estado)

        csv.exporta_csv()


if __name__ == "__main__":

    procesa_ds = ProcesaDataset()

    procesa_ds.genera_plantilla_dataset("lista_xml_dataset.csv", "*")

    # procesa_ds.generar_plantillas_conf_desde_lista("lista_xml_dataset.csv")
    # procesa_ds.procesar_lista_xml(
    #    "lista_xml_dataset.csv", OpcionProc.TOTAL.value)
