import pandas as pd
import lxml.etree as ET
import consulta_xsd as XSD

# Generar diccionario de doble clave, indexado por el campo que almacenan, con la relación entre cada código y su nombre
codigo_nombre = {}


class ConsultaNombres:
    '''
    Función para obtener el nombre del campo descriptor asociado a un código
    Si la columna es correspondiente a un nombre (universidad, titulación, municipio...), 
            su descriptor asociado tendrá el prefijo "Nombre".
    Si es un dato, el descriptor asociado tendrá el prefijo "Desc" (descripción)

    Recibe como parámetro el nombre de la columna en la que se basa
    '''

    def get_nombre_descriptor(self, campo: str):
        nombre_descriptor = ""
        sufijo = ""
        campo_principal = campo

        # Si el campo tiene un sufijo (Por ejemplo, Titulacion_1), separa prefijo y sufijo para buscar
        if "_" in campo:
            nombre_split = campo.split("_")
            campo_principal = nombre_split[0]
            sufijo = f"_{nombre_split[1]}"

        # lee la lista de campos que representan a un nombre
        with open("lista_campos_nombre.txt", "r") as fichero_campos_nombre:
            lista_campos_nombre = fichero_campos_nombre.read()

            # Busca el campo por su prefijo, pero añade el sufijo al nombre en caso de tenerlo

            # si el campo principal representa a un nombre, añade el prefijo "Nombre"
            if campo_principal in lista_campos_nombre:
                nombre_descriptor = f"Nombre{campo_principal}{sufijo}"
            else:
                # en caso contrario, añade el prefijo "Desc" (descripcion)
                nombre_descriptor = f"Desc{campo_principal}{sufijo}"

            return nombre_descriptor


class ConsultaTablas:
    def __init__(self):
        # Diccionario que contendrá los dataframes asociados a cada fichero auxiliar
        self.df_auxiliares = {}

    '''
	Funcion para buscar el nombre o descriptor asociado a un código, buscando en los diccionarios
		o en los ficheros auxiliares
		
	Recibe como parámetro el código a buscar, el campo al que pertenece, y en fichero donde buscar
	("dicc" para buscar en el diccionario)
	'''

    def busca_descriptor(self, campo: str, codigo: str, fichero: str, campo_descriptor: str = None):
        nombre = "N/A"

        if (not campo_descriptor) or (campo_descriptor == ""):
            # obten el posible nombre de su campo descriptor
            consulta_nombre = ConsultaNombres()
            campo_descriptor = consulta_nombre.get_nombre_descriptor(campo)

        # si el fichero es un XML, busca el nombre en su XSD
        if ".xsd" in fichero:
            consulta_xsd = XSD.ConsultaXSD(fichero)
            nombre = consulta_xsd.busca_descriptor_fichero(campo, codigo)
        elif (".XML" in fichero) or (".xml" in fichero):
            nombre = self.busca_descriptor_fichero_xml(
                campo, codigo, fichero, campo_descriptor)
        elif (".csv" in fichero):
            nombre = self.busca_descriptor_fichero_csv(
                campo, codigo, fichero, campo_descriptor)
        elif fichero == "dicc":
            nombre = self.busca_descriptor_dicc(campo, codigo)
        else:
            nombre = "N/A"

        return nombre

    '''
	Funcion para buscar el nombre o descriptor asociado a un código, dentro del conjunto de diccionarios
	Recibe como parámetro el código a buscar, y el campo al que pertenece
	'''

    def busca_descriptor_dicc(self, campo: str, codigo: int):
        tipo_codigo = codigo_nombre[campo]

        if codigo in tipo_codigo.keys():
            nombre = codigo_nombre[campo][codigo]
        else:
            nombre = "N/A"

        return nombre

    '''
	Funcion para buscar el nombre o desc asociado a un código, dentro de un fichero CSV
	Recibe como parámetro el código a buscar, y el campo al que pertenece
	'''

    def busca_descriptor_fichero_csv(self, campo: str, codigo: str, fichero: str, campo_descriptor: str):

        # Abre el dataframe asociado a ese fichero
        if codigo.isnumeric():
            codigo = int(codigo)
            df_fichero = pd.read_csv(fichero, sep=";")
        else:
            df_fichero = pd.read_csv(fichero, sep=";", dtype=str)

        # inicializa la variable nombre
        nombre = "N/A"

        '''
		A partir del código y el nombre de las columnas, busca el descriptor en el fichero auxiliar
		
		Para ello, busca en el fichero auxiliar la fila que contenga dicho código en el campo correspondiente.
		Una vez encontrado, busca dentro de la fila la columna con el descriptor correspondiente
		'''

        candidatos = df_fichero[campo_descriptor][df_fichero[campo] == codigo]
        if candidatos.any():
            nombre = candidatos.values[0]
        else:
            nombre = "N/A"

        return nombre

    '''
	Funcion para buscar el nombre o descriptor asociado a un código, dentro de los ficheros XML
	Recibe como parámetro el código a buscar, y el campo al que pertenece
	'''

    def busca_descriptor_fichero_xml(self, campo: str, codigo: str, fichero_xml: str, campo_descriptor: str):
        nombre = "N/A"

        xml = ET.parse(fichero_xml)
        raiz = xml.getroot()

        # busca un registro cuyo campo indicado tenga dicho codigo, y obten su descriptor
        busqueda = f"//{campo}[.='{codigo}']/../{campo_descriptor}"
        descriptor = raiz.xpath(busqueda)

        # si el descriptor existe, obten el nombre almacenado en el
        if descriptor:
            nombre = descriptor[0].text

        return nombre
