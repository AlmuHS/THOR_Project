import re
import glob

import variables_conf as var_conf

'''
Permite obtener el nombre y la ruta de diferentes ficheros necesarios
para el procesamiento del dataset
'''


class BuscaNombreFichero:

    '''
    Devuelve la ruta del fichero a procesar.
    Recibe por parámetro el nombre del fichero XML a procesar
    '''

    def obtener_ruta_fichero_procesar(self, nombre_fichero):
        ruta_fichero = f"{var_conf.RUTA_FICH_PROCESAR}/{nombre_fichero}"

        return ruta_fichero

    '''
    Calcula el nombre del fichero de configuracion necesario para procesar un XML.

    El fichero de configuración tendrá el mismo nombre que el XML, añadiendo el prefijo "_conf",
    y tendrá extensión .csv
    '''

    def obtener_nombre_fichero_conf(self, fichero_xml: str):
        nombre_sin_ext = fichero_xml.split(".")[0]
        fichero_config = f"{nombre_sin_ext}_conf.csv"

        return fichero_config

    '''
    Devuelve la ruta de un fichero de configuración a partir de su nombre
    Recibe por parámetro el nombre del fichero de configuración
    '''

    def obtener_ruta_fichero_conf(self, fich_config: str):
        ruta_config = f"{var_conf.RUTA_FICH_CONFIG}/{fich_config}"

        return ruta_config

    '''
	Calcula el nombre del fichero que almacenará el fichero XML ya procesado
	'''

    def obtener_nombre_fichero_procesado(self, fichero_entrada: str):
        # elimina la extension del nombre del fichero
        nombre_fichero = fichero_entrada.split(".")[0]

        # añade el sufijo y la extension al nombre del fichero
        nombre_fich_salida = f"{nombre_fichero}_desc.xml"

        return nombre_fich_salida

    '''
    Devuelve la ruta del fichero XML ya procesado
    Recibe por parámetro el nombre del fichero que almacenará el XML procesado
    '''

    def obtener_ruta_fichero_procesado(self, fichero_proc: str):
        ruta_proc = f"{var_conf.RUTA_FICH_PROCESADOS}/{fichero_proc}"

        return ruta_proc

    '''
    Obtiene el nombre del fichero XSD asociado a un XML
    Recibe por parámetro el nombre del fichero XML, o su ruta relativa desde el directorio actual

    Devuelve el nombre de su fichero XSD
    '''

    def obtener_fichero_xsd_xml(self, fichero_xml: str):
        # divide el nombre en directorio y fichero
        fichero_directorio = fichero_xml.split("/")

        # si el nombre incluye un directorio, el nombre del fichero será el último campo del array
        nombre_xml = fichero_directorio[-1]

        # borra el prefijo en el nombre del fichero
        nombre_filtrado1 = re.split(
            var_conf.COD_UNIVERSIDAD_ACTUAL, nombre_xml)[1]

        # borra el sufijo y la extension
        nombre_filtrado2 = re.split(r"_.*", nombre_filtrado1)[0]

        # obten el nombre del fichero XSD
        fichero_xsd = f"{nombre_filtrado2}.xsd"

        return fichero_xsd

    '''
    Obtiene la ruta donde se encuentra el fichero XSD asociado a un XML
    Recibe por parámetro la ruta relativa del fichero XML desde el directorio actual

    Devuelve la ruta relativa de su fichero XSD
    '''

    def obtener_ruta_xsd_xml(self, fichero_xml: str):
        # obten el directorio donde se guardan los ficheros XSD, desde su variable de configuración
        directorio = var_conf.RUTA_FICH_XSD

        # obten el nombre del fichero xsd
        nombre_xsd = self.obtener_fichero_xsd_xml(fichero_xml)

        # corrige la ruta del fichero, añadiendo su directorio
        ruta_fichero_xsd = f"{directorio}/{nombre_xsd}"

        return ruta_fichero_xsd

    '''
    Devuelve la ruta del fichero XSD.
    Recibe por parámetro el nombre del fichero XSD
    '''

    def obtener_ruta_fichero_xsd(self, fichero_xsd: str):
        ruta_xsd = f"{var_conf.RUTA_FICH_XSD}/{fichero_xsd}"

        return ruta_xsd

    '''
    Devuelve la ruta del fichero descriptor, donde se almacenan las relaciones de
    códigos y nombres. 

    Recibe por parámetro el nombre del fichero descriptor
    '''

    def obtener_ruta_fichero_descriptor(self, nombre_fichero: str):
        ruta_fichero_codigos = nombre_fichero

        if ".xsd" in nombre_fichero:
            ruta_fichero_codigos = self.obtener_ruta_fichero_xsd(
                nombre_fichero)
        else:
            ruta_fichero_codigos = f"{var_conf.RUTA_FICH_DESCRIPTOR}/{nombre_fichero}"

        return ruta_fichero_codigos

    '''
    Devuelve el nombre de cualquier fichero eliminando la ruta que le precede.
    Recibe por parámetro la ruta del fichero
    '''

    def obtener_fichero_sin_ruta(self, ruta_fichero: str):
        fich_dir = ruta_fichero.split("/")
        if fich_dir:
            nom_fichero = fich_dir[-1]
        else:
            nom_fichero = ruta_fichero

        return nom_fichero

    '''
    Devuelve el nombre de cualquier fichero eliminando su extensión
    Recibe por parámetro el nombre del fichero (sin ruta)
    '''

    def obtener_fichero_sin_ext(self, nombre_fichero: str):
        nombre_sin_ext = nombre_fichero.split(".")[0]

        return nombre_sin_ext

    '''
    Devuelve el nombre del dataset asociado a un fichero XML,
    consistente en el nombre de su fichero XSD sin su extensión

    Recibe por parámetro el nombre del fichero XML perteneciente al dataset
    '''

    def obtener_dataset_xml(self, fichero_xml: str):
        fichero_xsd = self.obtener_fichero_xsd_xml(fichero_xml)
        dataset = self.obtener_fichero_sin_ext(fichero_xsd)

        return dataset

    '''
    Devuelve el nombre del fichero de log asociado a un fichero XML
    Recibe por parámetro el nombre del fichero XML al que va asociado el log
    '''

    def obtener_fichero_log_xml(self, fichero_xml: str):
        nombre_sin_ruta = self.obtener_fichero_sin_ruta(fichero_xml)
        nombre_sin_ext = self.obtener_fichero_sin_ext(nombre_sin_ruta)
        nombre_log = f"{nombre_sin_ext}_log.csv"

        return nombre_log

    '''
    Devuelve la ruta del fichero de log asociado a un XML
    Recibe por parámetro el nombre del fichero de log
    '''

    def obtener_ruta_log(self, fichero_log: str):
        ruta_fich_log = f"{var_conf.RUTA_FICH_LOG}/{fichero_log}"

        return ruta_fich_log

    '''
    Devuelve la ruta del fichero de log, a partir del nombre de su fichero XML
    Recibe por parámetro el nombre del fichero XML al que va asociado el log
    '''

    def obtener_ruta_log_xml(self, fichero_xml: str):
        fich_log = self.obtener_fichero_log_xml(fichero_xml)
        ruta_fich_log = self.obtener_ruta_log(fich_log)

        return ruta_fich_log


class BuscaFichero:

    '''
    Comprueba si un fichero existe en la ruta indicada

    Recibe por parámetro la ruta del fichero a buscar
    Devuelve True si existe, False si no existe
    '''

    def comprueba_fich_existe(self, ruta_fichero: str):
        lista = glob.glob(f"{ruta_fichero}")

        return (len(lista) > 0)

    '''
    Busca ficheros que sigan un determinado patrón dentro del directorio indicado
    El patrón debe indicarse según una sintaxis similar a la de bash (*, .,?, ...)

    Recibe por parámetro el directorio donde buscar, y el patrón del nombre del fichero
    Devuelve una lista con los nombres de los ficheros que siguen ese patrón
    '''

    def busca_fich_patron(self, dir_fich: str, patron: str):
        lista_fich = glob.glob(f"{dir_fich}/{patron}")

        return lista_fich
