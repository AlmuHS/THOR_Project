# Proyecto THOR
## Herramienta para procesar conjuntos de datos SIIU en formato XML

## Introducción

Esta herramienta consiste en un conjunto de módulos programados para procesar conjuntos de ficheros XML, según el esquema de los ficheros de SIIU. Muchos de estos ficheros XML cuentan con campos "codificados" en los que un código reemplaza al valor textual del mismo. Estos campos codificados dificultan la lectura de los datos, y la comprensión de su contenido. 

Por esta razón, esta herramienta se ha diseñado para encontrar los valores textuales asociados a esos códigos, y añadir los mismos al fichero, ya sea reemplazando al campo original, o añadiendo un nuevo campo a continuación del mismo.

La herramienta también permite otras acciones, destinadas a anonimización total o parcial, en los ficheros que contienen datos personales. Estas otras acciones consisten en el borrado completo de ciertos campos, o el redondeo de años en las fechas.

## Requisitos

- Python 3
	+ LXML >= 4.5
	+ Pandas
	+ Glob
	

## Instalación

- **Descarga**

	Para descargar la herramienta, podéis descargar los fuentes usando Git
	
		git clone https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor.git
	
	Otra opción es descargar los fuentes en formato comprimido y extraerlos
	
	![](imagenes/descargar_codigo.png)

- **Instalación de dependencias**

	Una vez extraídos los fuentes, es necesario instalar las dependencias.  
	Para ello, abrimos una terminal, y nos situamos en el directorio donde hemos descargado los fuentes
	
		cd proyecto_thor
	
	Desde dentro del directorio, instalamos las dependencias con pip
	
		pip install -r requirements.txt
	
	En algunos sistemas, el pip de Python 3 es conocido como pip3. En estos sistemas, el comando quedaría así  
	  
	  	pip3 install -r requirements.txt

	Una vez resuelto esto, ya tendremos las dependencias necesarias para que nuestro proyecto funcione.
	

El proyecto no requiere instalación, pero los scripts hay que ejecutarlos desde el propio directorio donde están situados. Los ficheros a procesar, así como los ficheros adicionales que se requieran para procesarlos, deberán estar también situados dentro del mismo directorio.


## Uso básico de la herramienta

### Preparación del entorno de ejecución

Para que la herramienta funcione, es necesario preparar el entorno de ejecución con los ficheros y configuraciones necesarias para el funcionamiento de la misma.

#### Configuración de las rutas utilizadas por la herramienta

La herramienta toma ciertas rutas para la búsqueda de los ficheros necesarios para el procesamiento de los ficheros, así como para el almacenamiento los ficheros ya procesados.

Estas rutas se definen en el fichero `variables_conf.py`

	RUTA_FICH_DESCRIPTOR = "./auxiliares"
	RUTA_FICH_XSD = "./datos"
	RUTA_FICH_PROCESAR = "./datos"
	RUTA_FICH_CONFIG = "./datos"
	RUTA_FICH_PROCESADOS = "./datos"
	RUTA_FICH_LOG = "./datos"

- `RUTA_FICH_DESCRIPTOR` : Ruta donde se albergan los ficheros con los descriptores asociados a los códigos
- `RUTA_FICH_XSD`: Ruta donde se albergan los ficheros XSD asociados a los XML
- `RUTA_FICH_PROCESAR`: Ruta donde se albergan los ficheros a procesar
- `RUTA_FICH_CONFIG`: Ruta donde se albergan los ficheros de configuración asociados a cada fichero XML
- `RUTA_FICH_PROCESADOS`: Ruta donde se albergarán los ficheros ya procesados
- `RUTA_FICH_LOG`: Ruta donde se albergarán los ficheros de log, con los errores encontrados en el procesamiento de cada fichero

##### Otras configuraciones

- **Universidad actual**
	El fichero `variables_conf.py` incluye otras variables, que se van a utilizar para obtener algunos nombres de ficheros, como los de los XSD.
	
	Estas son:
	
		UNIVERSIDAD_ACTUAL = "UNIVERSIDAD DE HUELVA"
		COD_UNIVERSIDAD_ACTUAL = "049"
	
	Si queremos procesar los ficheros procedentes de otra universidad, deberemos cambiar la variable `COD_UNIVERSIDAD_ACTUAL` por el código identificativo de dicha universidad.

- **Nombres de campos descriptores**

	Por defecto, los campos descriptores que almacenen los valores textuales ya decodificados, se nombrarán con el nombre de su campo original, añadiendo el prefijo "Desc". 
	
	Pero, en algunos casos, puede ser útil indicar que el valor textual es un nombre propio. Para ello, en el fichero `lista_campos_nombre.txt` podemos añadir los nombres de los campos cuyo valor textual representa a un nombre. 
	En estos campos, el campo descriptor usará el prefijo "Nombre" añadido al nombre de su campo original.


#### Preparando los ficheros necesarios

Una vez configuradas las rutas, es necesario crear los directorios pertinentes y copiar los ficheros de cada uno de ellos.
Si nos basamos en la configuración por defecto, debemos crear dos directorios: `datos/` y `auxiliares/`

- En `datos/` debemos copiar los XML a procesar, junto a sus XSD. Deben ir todos dentro del mismo directorio, sin subdirectorios.  **Es importante mantener sus nombres originales, puesto que ciertas partes del programa obtienen el nombre del XSD a partir del nombre del XML.**

- En `auxiliares/` irán los ficheros csv extraídos de los ficheros Excel (.xls). Estos los podemos extraer a mano, o usando una función automática que explicaremos en el siguiente paso

##### Generando los CSV desde las hojas Excel

Los ficheros CSV los podemos generar a mano, abriendo los ficheros xls y extrayendo cada hoja en formato csv.
Pero esta tarea puede ser tediosa, por lo que nuestra herramienta proporciona una función para realizar este proceso de forma automática.

Para ello tenemos dos maneras:

1. **Desde la interfaz**

	Ejecutamos el script `menu.py`
	
		python3 menu.py
	
	Se nos abrirá un menú
	
		0.Generar ficheros CSV desde Excel
		1.Generar plantilla de dataset
		2.Generar plantillas de configuración desde dataset
		3.Procesar dataset
		Elija opción: 0
		Introduce la ruta del fichero XLS: 
		auxiliares/siiu_Codigos_RRHH.xls

	En el menú, seleccionamos la opción 0 y, cuando nos salga la pregunta, introducimos la ruta de nuestro fichero XLS (en nuestro caso, `auxiliares/siiu_Codigos_RRHH.xls`) y pulsamos enter.

	Si todo ha ido bien, el programa terminará, y generará una colección de ficheros csv en el directorio `auxiliares/` (u otro directorio, en caso de que hayamos cambiado la ruta en la variable  `RUTA_FICH_DESCRIPTOR`)
	
	Habrá que repetir este proceso por cada fichero que queramos extraer. 
	
2. **Desde el script**

	El script `genera_tablas.py` incluye una serie de llamadas de prueba, que se pueden utilizar para generar los ficheros csv.
	
	Para ello, debemos tener 4 ficheros: `siiu_Codigos_RRHH.xls`, `siiu_Codigos_becas.xls`, `siiu_Codigos_auxiliar.xls` y `siiu_Codigos_area académica.xls`, ubicados en el directorio `auxiliares/`.
	
	Una vez ubicados los ficheros, ejecutamos el script con
	
	`python3 genera_tablas.py`
	
	Tras esto, si todo ha ido bien, el programa debería generar una colección de ficheros csv en el directorio `auxiliares/`
	

Y con esto ya estaría el entorno de ejecución preparado para el procesamiento de los ficheros.

### Configuración de preprocesamiento

Una vez preparado el entorno con los ficheros y rutas necesarias, podemos empezar con los primeros pasos para la búsqueda de los ficheros a procesar, y la configuración de las acciones y rutas necesarias para su procesamiento.

#### Generación de la lista de ficheros del dataset

Para procesar el dataset en lote, es necesario generar la lista de ficheros, con sus ficheros de configuración y alguna información básica para su procesamiento.

Esto nos generará una plantilla, en formato csv, con una fila por cada fichero a procesar, indicando el nombre del fichero, su fichero de configuración, y el estado de procesamiento (inicialmente, a 1 = "Sin procesar"), el cual se actualizará tras el procesamiento del fichero.

Para ello, de nuevo, contamos con dos maneras: desde la interfaz, o invocando el script manualmente.

1. **Desde la interfaz**

	Volvemos a ejecutar el script `menu.py`
	
		python3 menu.py

	Esta vez seleccionamos la opción 1: Generar plantilla de dataset

		0.Generar ficheros CSV desde Excel
		1.Generar plantilla de dataset
		2.Generar plantillas de configuración desde dataset
		3.Procesar dataset
		Elija opción: 1
		Introduce nombre de la plantilla a generar: 
		lista_xml_dataset.csv
		¿Buscar todos los ficheros? (s/n)
		s

	Introducimos el nombre del fichero a generar. Será un fichero "plantilla" en formato csv. 
	Nos preguntará si queremos buscar todos los ficheros. 
	
	- Si queremos procesar todos los ficheros del directorio, pulsamos "s"
	- Si queremos procesar solo algunos ficheros, pulsamos "n". Esto nos preguntará por un "patrón" de nombre.  
		Este patrón seguirá un formato similar a las expresiones regulares, con esta sintaxis:
		
		- \* : Representa cualquier conjunto de caracteres
		- ? : Representa un sólo caracter
		- \[a-b]: Representa un rango entre `a` y `b`
	
		Podemos encontrar mas wildcards en este [enlace](https://facelessuser.github.io/wcmatch/glob/)
		
		Ejemplo: Buscar ficheros que contengan los caracteres RH (uno tras de otro).
			
			0.Generar ficheros CSV desde Excel
			1.Generar plantilla de dataset
			2.Generar plantillas de configuración desde dataset
			3.Procesar dataset
			Elija opción: 1
			Introduce nombre de la plantilla a generar: 
			lista_xml_dataset.csv 
			¿Buscar todos los ficheros? (s/n)
			n
			Introduce el patrón del nombre a buscar: 
			*RH*
			
2. **Desde el script**

	El script `procesa_dataset.py` incluye una serie de llamadas de prueba, que se pueden utilizar para generar las plantillas. Para ello, simplemente debemos ejecutar el script, con
	
		python3 procesa_dataset.py
		
Tras ejecutar esto, si todo ha ido bien, nos aparecerá un nuevo fichero en el directorio principal, con el nombre que hemos indicado. 

En el caso del ejemplo, o si lo hemos ejecutado desde el script, el nombre será `lista_xml_dataset.csv`

Si abrimos el fichero (podemos abrirlo usando LibreOffice), veremos algo como esto:

![](imagenes/plantilla_dataset.png)

Cada línea del fichero indicará la información necesaria para procesar el fichero, junto a algunos datos adicionales:

- **Dataset**: **Campo informativo.** Indica el nombre del dataset al que pertenece el fichero. 

- **Fichero_entrada**: El fichero que queremos procesar. Deberá estar ubicado en la ruta indicada en la variable `RUTA_FICH_PROCESAR` (en la configuración por defecto, será el directorio `datos/`).  **No modificar este campo**

- **Fichero_salida**: El fichero que almacenará el XML ya procesado. Deberá estar ubicado en la ruta indicada en la variable `RUTA_FICH_PROCESADOS`. Por defecto, el fichero tendrá el mismo nombre que el original, añadiendo el sufijo "_desc". Es posible cambiar este nombre por otro, en caso de necesidad.  

- **Fichero_config**: Este fichero almacenará la configuración necesaria para procesar el XML, indicando la acción a realizar por cada uno de sus campos, y donde buscar la información para decodificarlos dado el caso. **En el siguiente paso enseñaremos a generarlo**. Por defecto, tendrán el mismo nombre que el fichero original, pero con el sufijo "_conf" y la extensión ".csv". Si queremos que los ficheros de configuración tengan otro nombre, podemos editar este campo.

- **Estado**: El estado de procesamiento. Por defecto será 1="Sin procesar". Cuando el fichero haya sido procesado se actualizará este campo a 4="Procesado correcto" o 5="Procesado con errores". **No modificar este campo**

- **Error**: Si el procesamiento del fichero falla, se indicará el error en este campo.

**NOTA: Los nombres de fichero se indican sin sus rutas. Las rutas serán las indicadas en las variables de configuración.**


#### Generación de los ficheros de configuración

Una vez generada la lista de ficheros a procesar, necesitamos generar un fichero de configuración para cada uno de ellos. Este fichero de configuración indicará la acción a realizar con cada campo del XML y, en caso de ser un campo codificado, el fichero donde se encuentra la información para decodificarlo y los campos del fichero donde se almacenan el código y el valor decodificado.

Los ficheros de configuración se pueden generar automáticamente desde la herramienta. 

Para ello, podemos usar la interfaz:

Volvemos a ejecutar el script `menu.py`. 

	0.Generar ficheros CSV desde Excel
	1.Generar plantilla de dataset
	2.Generar plantillas de configuración desde dataset
	3.Procesar dataset
	Elija opción: 2
	Introduce el nombre de la plantilla del dataset: 
	lista_xml_dataset.csv

Seleccionamos la opción 2: Generar plantillas de configuración desde dataset.
Se nos pedirá el nombre de la plantilla del dataset. Este será el nombre del fichero generado en el paso anterior.
Introducimos el nombre y pulsamos Enter. Nos aparecerá una salida como esta

	generada plantilla para fichero U04918AX0101_02.XML
	generada plantilla para fichero U04918AC0105_03.XML
	generada plantilla para fichero U04918AC0305_02.XML
	generada plantilla para fichero U04918AC0403_03.XML
	generada plantilla para fichero U04918AX0204_02.XML
	generada plantilla para fichero U04918RH0201_01.XML
	generada plantilla para fichero U04918AX0203_02.XML
	generada plantilla para fichero U04918AC0402_02.XML
	generada plantilla para fichero U04918AC0313_03.XML
	generada plantilla para fichero U04918RH0301_05.XML
	generada plantilla para fichero U04918AC0301_06.XML
	generada plantilla para fichero U04918AC0201_04.XML

Tras esto, nos deberían haber aparecido en el directorio `datos/` (o en el directorio que hayamos indicado en `RUTA_FICH_CONFIG`) una serie de ficheros con el sufijo "_conf" y extensión .csv (salvo que hayamos modificado su nombre en la plantilla del dataset).

Los ficheros resultantes serán similares a este:

![](imagenes/fichero_config.png)

El fichero de configuración contendrá los siguientes campos:

- **Campo:** Nombre del campo a procesar
- **Acción: ** La acción a realizar con dicho campo. Admite los valores:
	+ **_"describir"_:** añade un campo descriptor con el valor textual de cada código
	+ **_"reemplazar"_:** reemplaza el campo original codificado, por el valor textual de cada código
	+ **_"borrar"_:** borra el campo
	+ **_"redondear"_:** **solo aplicables para campos de año**, redondea el año a su década mas cercana (1987 -> 1990, 1982 -> 1980)
	+ **_"mantener"_:** no realiza ninguna acción, manteniendo el campo en sus valores originales
- **Campo que referencia:** Dentro del fichero donde lo vamos a consultar, el campo que almacena los códigos referentes a este.
- **Campo descriptor:** Dentro del fichero donde lo vamos a consultar, el campo que almacena el valor textual asociado al código
- **Fichero que referencia:** Fichero donde vamos a consultar esta información. Puede ser XML, CSV o XSD.  

El fichero también incluye algunos campos más, para facilitar el completado de su configuración:

- **Describible:** Indica si el campo se corresponde a un campo codificado, que se pueda describir con un valor textual. **Campo informativo. No editar**
- **Observaciones:** Copia del contenido del *description* de su fichero XSD, explicando en qué consiste dicho campo, y lo que almacena. **Campo informativo. Su edición será ignorada**

Las columnas "Observaciones" y "Describible" se rellenarán automáticamente a partir del contenido del fichero XSD asociado al XML. 

Asimismo, en caso de que el campo esté autodescrito dentro del XSD, las columnas "Fichero que referencia" y "Campo que referencia" se rellenarán con los datos del fichero XSD, y la "Acción" será definida a "describir". 

Esta misma función también detectará automáticamente los campos a borrar, mediante la lista de campos indicada en el fichero `campos_para_borrar.txt`. Para estos campos, la columna "Acción" de su fichero de configuración será establecida a "borrar".

Para los campos que no estén autodescritos dentro del XSD, y que no estén en la lista de campos a borrar, su acción será establecida a "mantener".

**Esta configuración por defecto puede ser modificada por el usuario posteriormente, editando el fichero de configuración**

**NOTA: Al igual que con la plantilla del dataset, en el fichero de configuración los nombres de fichero se indican sin rutas. Las rutas serán las indicadas en las variables de configuración**

Una vez generado, habrá que editarlo manualmente para añadir la configuración de aquellos campos que no se hayan rellenado automáticamente, o para modificar la configuración por defecto. 

#### Editando los ficheros de configuración

Los ficheros de configuración que genera nuestra herramienta son plantillas con alguna información básica.
Pero, en su estado inicial, solo procesarían aquellos campos cuyos códigos estén dentro del fichero XSD, o aquellos que aparezcan en la lista de campos para borrar.

Para realizar un procesamiento mas fino, debemos editarlos, modificando las acciones en algunos de los campos, y añadiendo ficheros donde buscar la información. Para ello, podemos seguir los siguientes consejos:

- El campo _"Describible"_ indica si el campo indicado está codificado. Si su valor es "si", significa que podemos asociar un valor textual a dicho campo. **Es un campo informativo, aunque no siempre es fiable. Si queremos saber con mas precisión el contenido de dicho campo, podemos mirar su descripción en "Observaciones"**.  

-  Si queremos decodificar un campo, debemos rellenar las columnas "Campo que referencia", "Campo descriptor" y "Fichero que referencia". 
	- El fichero que referencia puede ser un CSV, XML o un XSD. El nombre del fichero no debe incluir la ruta
	- El campo que referencia hace relación al campo que almacena el código dentro del fichero que referencia.
	- El campo descriptor hace relación al campo que almacena el valor textual asociado a dicho código, en el fichero que referencia. En el fichero XSD, este campo puede estar vacío (en el XSD, el valor textual se almacena junto a la descripción del campo).

- Para rellenar algunos campos conocidos, disponemos del fichero [`campos_comunes.csv`](https://gitlab.uhu.es/almudena.juradocenturion/scripts_ckan/blob/master/campos_comunes.csv), en el cual se indican los valores a rellenar en cada columna para cada uno de esos campos. Para añadirlos al fichero de configuración, simplemente tenemos que copiar, de la fila que nos interese, las columnas de la segunda a la última, y pegarlos en su fila correspondiente del fichero de configuración.

**Ejemplo: Rellenando la configuración del campo "Universidad" a partir del listado de campos comunes**

Copiamos la fila correspondiente en el fichero de campos comunes
![](imagenes/campo_comun_universidad.png)

Lo copiamos en el fichero de configuración, y cambiamos la acción a "describir"
![](imagenes/rellena_config_universidad.png)

**NOTA: ¡¡No olvides cambiar el valor de su campo Acción!!**

Una vez editado el fichero de configuración, se verá de forma parecida a este:

![](imagenes/fichero_config_completo.png)

Se puede apreciar como ahora, en los "Ficheros que referencia" aparecen ficheros csv además de los XSD.

**Habrá que repetir esta labor por cada fichero que queramos procesar en nuestro dataset, modificando el fichero de configuración de cada uno de ellos**

**NOTA 2: En caso de que el código a buscar no se encuentre en el fichero indicado, la herramienta repetirá la búsqueda utilizando el fichero XSD, obteniendo su nombre a partir del nombre del fichero XML. Por esta razón, es importante no renombrar estos ficheros**

### Procesamiento del dataset

Una vez completada la configuración de los ficheros a procesar, podemos comenzar el procesamiento del dataset.

Para ello, podemos usar la interfaz, ejecutando el script `menu.py` y pulsando en la opción 3: Procesar dataset

	python3 menu.py

	0.Generar ficheros CSV desde Excel
	1.Generar plantilla de dataset
	2.Generar plantillas de configuración desde dataset
	3.Procesar dataset
	Elija opción: 3
	Introduce el nombre de la plantilla del dataset: 
	lista_xml_dataset.csv
	¿Desea procesar todos los ficheros? (s/n)
	s

Se nos preguntará si queremos procesar todos los ficheros. Pulsamos "s" para procesarlos todos.
En caso contrario, solo se procesarán los ficheros que estén marcados como "procesados con errores (Estado=5)"

Pulsamos Enter para iniciar el procesamiento del dataset

Se nos mostrará una salida similar a esta

![](imagenes/procesando_dataset.png)

El proceso durará varios minutos, dependiendo del número de ficheros a procesar y de la cantidad de registros incluidos en los mismos.

Tras finalizar el procesado, deberán aparecer en nuestro directorio datos/ (o en el que hayamos indicado en la variable `RUTA_FICH_PROCESADOS`), una colección de ficheros con el sufijo "_desc" (salvo que en la plantilla del dataset indicáramos otro nombre). Estos serán los ficheros ya procesados.

Si abrimos uno de esos ficheros con el editor de texto, podremos ver sus campos ya procesados.

![](imagenes/fichero_xml_procesado.png)

Los campos decodificados aparecerán con el prefijo "Desc" seguido del nombre de su campo original. Salvo en los campos que estén apuntados en el fichero `lista_campos_nombre.txt`, que usarán el prefijo "Nombre" (asumiendo que estos campos se corresponden a nombres propios).


### Revisando logs

Durante el procesamiento de los ficheros, la herramienta irá generando una serie de logs. Estos logs irán almacenando los errores encontrados durante el procesamiento de cada fichero.

#### Revisando el estado del dataset

Lo primero que podemos revisar al terminar el procesamiento es la plantilla del dataset, aquella que creamos en el primer paso y que contiene la lista de ficheros a procesar.

En esta plantilla, al terminar el procesamiento de cada fichero, se actualiza su estado según el resultado del mismo, y se indican algunos errores (normalmente referentes a la falta de algún fichero).

Abrimos la plantilla del dataset, y vemos un resultado similar a este:

![](imagenes/estado_dataset.png)

La columna "Estado" indica el estado de procesamiento:

- **1 = No procesado**
- **4 = Procesado correctamente**
- **5 = Procesado con errores**

En este caso vemos que se han procesado todos los ficheros, aunque hay algunos con errores. En caso de aparecer algún error previo al procesamiento, este se mostrará en el campo "Error"

![](imagenes/estado_dataset_error.png)

En este caso, podemos ver como en los dos últimos ficheros, no se han encontrado los ficheros necesarios

#### Revisando logs de ficheros individuales

Una vez detectamos que hay ficheros procesados con errores, revisamos sus logs para encontrar el error.

En el directorio `datos/` (o en el que indique la variable `RUTA_FICH_LOG`), podemos encontrar los logs de procesamiento de cada fichero, que tendrán el mismo nombre que el original, con el sufijo "_log" y extensión .csv

**En caso de que el fichero se haya procesado sin errores, el log aparecerá vacío**

Abrimos uno de ellos y, en caso de contener errores, veremos algo como esto:

![](imagenes/log_fichero_error.png)

El fichero de log contendrá los siguientes campos:

- **Fichero**: Nombre del fichero donde se ha encontrado el error
- **Registro:** Número de registro donde se ha encontrado el error (los ficheros XML se estructuran en registros, no en filas)
- **Error:** Descripción del error encontrado


En este caso, los errores se corresponden a códigos de campos que no han sido encontrados dentro de sus ficheros descriptores.

También podemos encontrar otro tipo de errores:

![](imagenes/fichero_log_error2.png)

En este caso, por un error en la configuración, se ha indicado que se redondee un campo que no se corresponde a un año. En el log se nos muestran errores indicando que dichos valores no son años válidos.

![](imagenes/fichero_log_error3.png)

En este otro caso, por otro error en la configuración, la acción para el campo "AreaConocimiento" no ha sido definida.


Siguiendo estos logs, podremos encontrar errores en la configuración o en el procesamiento


## Funcionamiento interno

La herramienta se compone de varios componentes:

### Componentes principales 

- **Motor de consultas:** Componente encargado de buscar el valor textual correspondiente a cada tipo de código. El valor textual será conocido como *"descripción"*, y al campo que almacena dicho valor textual *"descriptor"*.  El motor de consultas soporta búsquedas en ficheros de 3 formatos diferentes: CSV, XML, y XSD (el esquema del XML a procesar). 
  
  Compuesto por varios ficheros:
  
  - [`consulta_datos.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/consulta_datos.py): Fichero principal del motor de consultas. Compuesto por dos clases:
	  + `ConsultaNombre`: clase destinada a consultar nombres de campos. Actualmente permite obtener el nombre del campo descriptor asociado a un campo de código.
	  + `ConsultaTablas`: Permite decodificar un campo de código, encontrando su valor textual en los ficheros auxiliares. Incluye funciones para XML y CSV (las de XSD están en una clase separada); y una función principal que llama a una o otra dependiendo de la extensión del fichero que reciba.
  - [`consulta_xsd.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/consulta_xsd.py):  Fichero destinado a obtener información de un fichero XSD
	
	- `ConsultaXSD`: Clase principal de este fichero. Permite obtener información de un fichero XSD, entre ellas el valor textual asociado a un código.
		
	  - **Lista de campos almacenados en el XML:** Esta función también obtiene alguna información adicional, como la de si el campo es *"describible"* o no (si contiene un código que se pueda traducir a un valor textual), el contenido de su campo *description* (explicando qué es lo que almacena dicho campo), y si está autodescrito dentro del propio fichero XML.  
	  		
	  - **Búsqueda de descripciones:** Permite buscar la descripción de un código dentro del fichero XSD. Esta se puede encontrar en enumeraciones, o extraerse mediante expresiones regulares a partir del campo *description* (esta última será muy útil para encontrar codigos "de excepción", para casos donde hay valores "no válidos" o que "no aplican")
  
 - **Procesador de ficheros XML**:  Componente encargado de realizar el procesamiento de los ficheros XML. Compuesto por varios módulos:
 
	- [`procesa_dataset.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/procesa_dataset.py): Fichero principal, encargado de iniciar el procesamiento de conjuntos de ficheros.   
		Incluye funciones para crear la plantilla con la lista de ficheros a procesar, generar el conjunto de ficheros de configuración para cada uno de ellos, e iniciar el procesamiento de los ficheros indicados en la plantilla (comprobando ciertas condiciones de error y reportándolas en el log y en el estado del fichero).
		
		- [`procesa_xml.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/procesa_xml.py): Fichero encargado del procesamiento de ficheros XML individuales. La clase `ProcesaXML` recibe en su constructor el nombre del fichero a procesar, e implementa los métodos para crear la plantilla de su fichero de configuración, y realizar el procesamiento del fichero.
			+ [`procesa_accion.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/procesa_accion.py): Fichero auxiliar, encargado de implementar las acciones de procesamiento del fichero. Cada acción está implementada en una única clase, heredada de la clase `ProcesaAccion`. Este esquema permite la invocación de las acciones (desde `procesa_xml.py`) mediante el uso de un diccionario de clases, sin necesidad de usar un switch. Devuelve diferentes códigos, en función de si el resultado ha sido exitoso o los errores encontrados.

- **Manejador de logs**: Componente encargado de gestionar los logs, añadiendo los errores encontrados durante el procesamiento del fichero. Implementado en el fichero [`genera_log.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/genera_log.py)

	- `ManejaLogger`: clase encargada de crear el log (o cargar uno ya existente), añadir los errores, y exportarlo a un fichero csv. Recibe en su constructor el nombre del fichero a crear o cargar.   
	
	- `GeneraError`: Estructura de clases que, haciendo uso de la clase anterior, se encargan de generar los mensajes para los diferentes tipos de errores, y añadirlos al fichero de log. Siguen un patrón similar al de `ProcesaAccion`, con múltiples clases que heredan de `GeneraError`, permitiendo la invocación de sus tareas mediante un diccionario de objetos, sin necesidad de usar un switch.

### Componentes auxiliares

- **Manejador de ficheros CSV**: Componente encargado de realizar algunas acciones de lectura o escritura en ficheros XML, basándose en la librería pandas. Implementado en el fichero [`maneja_csv.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/maneja_csv.py).  

- **Búsqueda de rutas y ficheros**: Componente encargado de obtener las rutas y los nombre de los ficheros necesarios por el programa.
	+ [`variables_conf.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/variables_conf.py): Fichero encargado de definir las variables con las rutas para cada tipo de fichero, y otros datos necesarios para los mismos, como el código de la universidad actual (el cual se utilizará para obtener el nombre del fichero XSD, a partir del fichero XML).
	+ [`busca_fichero.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/busca_fichero.py): Módulo encargado de obtener los nombres de fichero mediante patrones conocidos, y comprobar su existencia dentro del sistema. Obtiene las rutas de los ficheros mediante las variables indicadas en `variables_conf.py`. 
		* `BuscaNombreFichero`: clase encargada de buscar los nombres y rutas de los ficheros a utilizar por el programa. Permite obtener el nombre del fichero XSD a partir del nombre del fichero XML, obtener el nombre para el fichero de configuración (añadiendo "_conf" al fichero original)... etc.  También permite obtener las rutas de dichos ficheros, concatenando sus nombres con las  ruas indicadas en `variables_conf.py`  
		
		* `BuscaFichero`: Permite buscar ficheros dentro del sistema, buscando listas de ficheros que siguen un patrón dentro de su nombre, o simplemente comprobando que un fichero existe en la ruta indicada por el usuario.  
		
- **Generador de ficheros auxiliares**: Componente encargado de generar los ficheros descriptores a partir de los ficheros auxiliares del SIIU en formato XLS, extrayendo sus hojas en formato csv. Implementado en el fichero [`genera_tablas.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/genera_tablas.py).

### Interfaz

- **Menú en modo texto: ** Implementado en el fichero [`menu.py`](https://gitlab.uhu.es/almudena.juradocenturion/proyecto_thor/blob/master/menu.py), permite la realización de tareas de procesamiento del dataset, de una manera mas sencilla, mediante menús de texto y preguntas al usuario.

	
	Formado por la función principal, `menu()`, y una estructura de clases similar a la de `ProcesaAcción` y `GeneraError`, basada en la clase principal `OpcionMenu`y un conjunto de clases que heredan de la misma, implementado cada una de las opciones del menú. Las opciones del menú se ejecutan mediante un diccionario de objetos, que incluye un objeto de cada opción asociado al número de opción de cada uno de ellos.
	
	