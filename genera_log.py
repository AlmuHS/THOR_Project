import lxml.etree as ET
import enum
import glob


import busca_fichero as busca_fich
import procesa_accion as pr_accion
import maneja_csv as man_csv


class ManejaLogger:
    csv_log: man_csv.ManejaCSV
    fichero_log: str
    fichero_xml = str

    def __init__(self, fichero_xml: str):
        busca_nom_fich = busca_fich.BuscaNombreFichero()

        self.fichero_xml = busca_nom_fich.obtener_fichero_sin_ruta(fichero_xml)
        self.fichero_log = busca_nom_fich.obtener_ruta_log_xml(fichero_xml)
        self.csv_log = man_csv.ManejaCSV(self.fichero_log)
        self.crea_log()

    def crea_log(self):
        # crea las columnas del fichero
        lista_columnas = ["Fichero", "Registro", "Error"]
        self.csv_log.crea_df_columnas(lista_columnas)

    def cargar_log(self, fichero_log: str):
        self.csv_log.carga_fichero()

    def anadir_error(self, num_registro: str, error: str):
        dict_fila = {"Fichero": [self.fichero_xml],
                     "Registro": [num_registro],
                     "Error": [error]}
        self.csv_log.anadir_fila(dict_fila)

    def exportar_log(self):
        # exporta el dataframe a csv
        self.csv_log.exporta_csv()


class GeneraError:

    def __init__(self, logger: ManejaLogger):
        self.logger = logger

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        pass


class GeneraErrorNoAccion(GeneraError):
    def __init__(self, logger: ManejaLogger):
        super().__init__(logger)

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        campo = campo_reg.tag
        mensaje = f"Acción para campo {campo} no definida"

        self.logger.anadir_error(num_registro, mensaje)


class GeneraErrorNoDesc(GeneraError):

    def __init__(self, logger: ManejaLogger):
        super().__init__(logger)

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        codigo = campo_reg.text
        campo = campo_reg.tag
        mensaje = f"Codigo {codigo} de campo {campo} no encontrado"

        self.logger.anadir_error(num_registro, mensaje)


class GeneraErrorNoNum(GeneraError):
    def __init__(self, logger: ManejaLogger):
        super().__init__(logger)

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        dato = campo_reg.text
        campo = campo_reg.tag
        mensaje = f"El dato {dato} del campo {campo} no es un valor numérico"

        self.logger.anadir_error(num_registro, mensaje)


class GeneraErrorNoAnio(GeneraError):
    def __init__(self, logger: ManejaLogger):
        super().__init__(logger)

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        dato = campo_reg.text
        campo = campo_reg.tag
        mensaje = f"El dato {dato} del campo {campo} no es un año válido"

        self.logger.anadir_error(num_registro, mensaje)


class GeneraErrorNoFichero(GeneraError):
    def __init__(self, logger: ManejaLogger):
        super().__init__(logger)

    def anade_error_log(self, campo_reg: ET.ElementTree, num_registro: int):
        campo = campo_reg.tag
        mensaje = f"El campo {campo} no tiene fichero de configuración asociado"

        self.logger.anadir_error(num_registro, mensaje)
