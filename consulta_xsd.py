import re
import lxml.etree as ET


class ConsultaXSD:
    fichero_xsd: str
    xsd_tree: ET.Element = None
    principal: ET.Element = None
    campos_xsd: ET.Element = None

    def __init__(self, fichero_xsd: str):
        self.fichero_xsd = fichero_xsd
        self.__lee_campos(fichero_xsd)

    '''
	Funcion auxiliar para encontrar los campos del fichero XSD, y cargarlos como atributo de la clase
	'''

    def __lee_campos(self, fichero_xsd: str):
        self.xsd_tree = ET.parse(fichero_xsd)
        self.principal = self.xsd_tree.find(
            ".//{http://www.w3.org/2001/XMLSchema}element[@name='Registro']")

        busqueda_campos = f".//{{http://www.w3.org/2001/XMLSchema}}element"
        self.campos_xsd = self.principal.findall(busqueda_campos)

    '''
	Funcion para obtener la lista de campos de un fichero XSD.
	Recibe por parámetro la ruta del fichero XML a procesar.
	Devuelve un diccionario con el nombre del campo y sus atributos (describible, descrito...)
	'''

    def obten_lista_campos(self):
        lista_campos = {}

        for campo in self.campos_xsd:
            # lista_campos.append(campo.attrib["name"])
            nombre_campo = campo.attrib["name"]
            descripcion = campo.find(
                ".//{http://www.w3.org/2001/XMLSchema}documentation").text

            atributos_campo = {}

            atributos_campo["Campo"] = nombre_campo
            atributos_campo["Observaciones"] = descripcion

            if ("Código" in descripcion) or ("código" in descripcion) or ("[" in descripcion):
                atributos_campo["Describible"] = "si"
            else:
                atributos_campo["Describible"] = "no"

            enum = campo.find(
                ".//{http://www.w3.org/2001/XMLSchema}enumeration")

            # si tiene enumeraciones, añade el nombre del campo a la lista de campos descritos
            if (enum is not None) or ("[" in descripcion):
                atributos_campo["Descrito"] = "si"
            else:
                atributos_campo["Descrito"] = "no"

            lista_campos[campo] = atributos_campo

        return lista_campos

    '''
	Funcion para buscar el nombre o descripción asociado a un código, dentro del fichero XSD
	asociado a un XML

	Recibe por parámetro el código a buscar, el campo al que pertenece, y la ruta del fichero XML 
	a describir (incluyendo directorio)

	Devuelve el nombre en caso de encontrarlo, N/A en otro caso
	'''

    def busca_descriptor_fichero(self, campo: str, codigo: str):
        # inicializa la variable nombre
        nombre = "N/A"

        # busca entre los campos Element, el que se corresponde con el nuestro
        busqueda_elem = f".//{{http://www.w3.org/2001/XMLSchema}}element[@name='{campo}']"
        element = self.xsd_tree.find(busqueda_elem)

        if element is not None:
            # En el campo Element, busca la enumeracion que se corresponde con nuestro codigo
            busqueda_enum = f".//{{http://www.w3.org/2001/XMLSchema}}enumeration[@value='{codigo}']"
            enumeration = element.find(busqueda_enum)

            descripcion = element.find(
                ".//{http://www.w3.org/2001/XMLSchema}documentation").text

            # si la enumeracion exite, accede a su campo documentation y obten su texto asociado
            if enumeration is not None:
                documentation = enumeration.find(
                    ".//{http://www.w3.org/2001/XMLSchema}documentation")
                nombre = documentation.text

            # si no encuentras el código, o no hay enumeracion, busca en la descripcion del campo
            if (nombre == "N/A") and (("[" in descripcion) or ("=" in descripcion)):
                nombre = self.__busca_descriptor_descripcion(
                    descripcion, codigo)

        return nombre

    '''
	Funcion para buscar el nombre o descriptor asociado a un código, 
	buscando dentro de una cadena 'description' dentro del XSD, a partir de ciertos patrones conocidos
	
	Recibe por parámetro la cadena con el contenido del campo 'description' del XSD, y el código a buscar
	'''

    def __busca_descriptor_descripcion(self, descripcion: str, codigo: str):
        nombre = "N/A"

        # busca el patron tipo "0=[No] , 1=[Si]" y similares
        patron = re.findall(r"\d=\[\D*\]", descripcion)

        if patron:
            for par in patron:
                clave_valor = par.split('=')
                clave = clave_valor[0]
                valor = clave_valor[1]

                valor_fix = valor.replace('[', '').replace(']', '')

                if codigo == clave:
                    nombre = valor_fix
                    break
        if nombre == "N/A":
            # busca el patron tipo "[H] Hombre, [M] Mujer" y similares
            patron = re.findall(r'\[\S\], \S*', descripcion)

            for par in patron:
                clave_valor = par.split(" ")

                clave = clave_valor[0]
                valor = clave_valor[1]

                clave_fix = clave.replace('[', '').replace(']', '')
                valor_fix = re.sub(r"\W", '', valor)

                if codigo == clave_fix:
                    nombre = valor_fix
                    break
        if nombre == "N/A":
            # busca el patron "[0]=No existe  , [1]=Sí." y similares
            patron_comp = re.compile(r'\[(\d)\]\=([^\,\.]+)')
            patron = patron_comp.findall(descripcion)

            for par in patron:
                clave = par[0]
                valor = par[1]

                clave_fix = re.sub(r"\s", '', clave)

                if codigo == clave_fix:
                    valor_fix = re.sub(" , ", '', valor)
                    nombre = valor_fix
                    break

        if nombre == "N/A":
            # Busca el patron de tipo 8=No aplica.
            patron = re.findall(r"\d*\=\D*[\.| y ]\W", descripcion)

            for par in patron:
                clave_valor = par.split("=")

                clave = clave_valor[0]
                valor = clave_valor[1]

                valor_fix = valor.replace(" y ", "")
                valor_fix = re.sub(r"\W*$", '', valor_fix)

                if codigo == clave:
                    nombre = valor_fix
                    break

        return nombre
