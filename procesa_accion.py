import enum
import math

import lxml.etree as ET
import pandas as pd

import busca_fichero as busca_fich
import consulta_datos as CD
import maneja_csv as man_csv
import variables_conf as var_conf


class Resultado(enum.Enum):
    CORRECTO = 0
    ERROR_NO_DESCRIPCION = 1
    ERROR_NO_CAMPO = 2
    ERROR_NO_NUMERO = 3
    ERROR_NO_FICHERO = 4
    ERROR_NO_ANIO = 5
    ERROR_NO_ACCION = 6


class ProcesaAccion:
    def procesa(self, registro, fila_conf, campo_reg, fich_xml):
        pass

    '''
	Obtiene el nombre o descriptor de un campo, a partir de los datos indicados en su configuración

	Recibe por parámetro la fila del fichero de configuración correspondiente a dicho campo, el registro de ese campo dentro del dataframe,
	y el fichero_xml del que procede dicho campo (necesario si no encontramos su descriptor en el fichero indicado)

	Devuelve el nombre o descriptor del campo, o "N/A" si no lo encuentra
	'''

    def obtener_descripcion_campo(self, fila_conf: pd, campo_reg: pd, fichero_xml: str):
        # descriptor del campo
        nombre = "N/A"

        busca_nom_fich = busca_fich.BuscaNombreFichero()

        # nombre del campo
        nombre_campo = campo_reg.tag

        # codigo almacenado en dicho campo
        codigo = campo_reg.text

        # nombre del campo donde vamos a buscar la información
        campo_que_referencia = fila_conf["Campo que referencia"].values[0]

        # nombre del fichero donde vamos a buscar la información
        fichero_donde_buscar = fila_conf["Fichero que referencia"].values[0]

        # ruta del fichero donde vamos a buscar la informacion
        ruta_fichero_buscar = busca_nom_fich.obtener_ruta_fichero_descriptor(
            fichero_donde_buscar)

        # nombre del campo que almacena el descriptor dentro del fichero
        campo_descriptor = fila_conf["Campo descriptor"].values[0]

        # busca el nombre correspondiente al código
        consulta_dato = CD.ConsultaTablas()
        nombre = consulta_dato.busca_descriptor(
            campo_que_referencia, codigo, ruta_fichero_buscar, campo_descriptor)

        # si no encuentras el código en la tabla correspondiente, busca en su XSD (esto es para detectar los códigos de excepciones)
        if nombre == "N/A":
            busca_fichero = busca_fich.BuscaNombreFichero()
            fichero_xsd = busca_fichero.obtener_ruta_xsd_xml(fichero_xml)
            nombre = consulta_dato.busca_descriptor(
                nombre_campo, codigo, fichero_xsd)

        return nombre


class ProcesaBorrar(ProcesaAccion):
    def procesa(self, registro: ET.ElementTree, fila_conf: pd.DataFrame, campo_reg: ET.ElementTree, fich_xml: str):
        registro.remove(campo_reg)

        return Resultado.CORRECTO


class ProcesaDescribir(ProcesaAccion):
    '''
    Inserta un nuevo campo XML en el registro indicado, con la descripcion del campo recibido por parámetro

    Recibe por parámetro el registro donde insertar el campo, el campo a partir del cual generamos la descripcion
    y el nombre a almacenar en el descriptor
    '''

    def _insertar_campo_descrito_xml(self, registro: ET.Element, campo_reg: ET.Element, nombre: str):
        # nombre del campo
        campo = campo_reg.tag

        # crea nuevo campo descriptor
        consulta_nombre = CD.ConsultaNombres()
        nom_desc = consulta_nombre.get_nombre_descriptor(campo)
        desc_reg = ET.Element(nom_desc)

        # busca el indice donde se insertará el nuevo campo, a continuación de su campo base
        posicion = registro.index(campo_reg) + 1

        # rellena el campo descriptor con la descripción o nombre correspondiente
        desc_reg.text = nombre

        # inserta el nuevo campo descriptor en la posición correspondiente del registro
        registro.insert(posicion, desc_reg)

    def procesa(self, registro: ET.ElementTree, fila_conf: pd.DataFrame, campo_reg: ET.ElementTree, fich_xml: str):

        # nombre del fichero donde vamos a buscar la información
        fichero_donde_buscar = fila_conf["Fichero que referencia"].values[0]

        # Obten la ruta del fichero anterior
        busca_nom_fich = busca_fich.BuscaNombreFichero()
        ruta_donde_buscar = busca_nom_fich.obtener_ruta_fichero_descriptor(
            fichero_donde_buscar)

        # Comprueba si el fichero existe dentro del sistema
        busca_fichero = busca_fich.BuscaFichero()
        fichero_existe = busca_fichero.comprueba_fich_existe(
            ruta_donde_buscar)

        # solamente describe el campo si tiene definido un fichero donde buscar el codigo
        if fichero_donde_buscar and fichero_existe:
            # busca el nombre asociado a ese código
            nombre = super().obtener_descripcion_campo(fila_conf, campo_reg, fich_xml)

            # inserta el nuevo campo descrito en el fichero XML
            self._insertar_campo_descrito_xml(registro, campo_reg, nombre)

            if nombre == "N/A":
                return Resultado.ERROR_NO_DESCRIPCION
        else:
            return Resultado.ERROR_NO_FICHERO

        return Resultado.CORRECTO


class ProcesaReemplazar(ProcesaAccion):
    def procesa(self, registro: ET.ElementTree, fila_conf: pd.DataFrame, campo_reg: ET.ElementTree, fich_xml: str):
        # nombre del fichero donde vamos a buscar la información
        fichero_donde_buscar = fila_conf["Fichero que referencia"].values[0]

        # Obten la ruta del fichero anterior
        busca_nom_fich = busca_fich.BuscaNombreFichero()
        ruta_donde_buscar = busca_nom_fich.obtener_ruta_fichero_descriptor(
            fichero_donde_buscar)

        # Comprueba si el fichero existe dentro del sistema
        busca_fichero = busca_fich.BuscaFichero()
        fichero_existe = busca_fichero.comprueba_fich_existe(
            ruta_donde_buscar)

        # solamente describe el campo si tiene definido un fichero donde buscar el codigo
        if fichero_donde_buscar and fichero_existe:
            # busca el nombre asociado a ese código
            nombre = super().obtener_descripcion_campo(fila_conf, campo_reg, fich_xml)

            # reemplaza el código almacenado en el campo, por su descripcion
            campo_reg.text = nombre

            if nombre == "N/A":
                return Resultado.ERROR_NO_DESCRIPCION
        else:
            return Resultado.ERROR_NO_FICHERO

        return Resultado.CORRECTO


class ProcesaRedondear(ProcesaAccion):
    def procesa(self, registro: ET.ElementTree, fila_conf: pd.DataFrame, campo_reg: ET.ElementTree, fich_xml: str):
        # obten el valor almacenado en dicho campo
        valor = campo_reg.text

        # antes de redondear, comprueba que el valor realmente es un número
        if valor.isnumeric():
            # convertimos la cadena a valor entero para poder redondear su valor
            num_valor = int(valor)

            # inicializamos la variable redondeo al numero original
            redondeo = num_valor

            # solo aplica el redondeo si el año es válido
            if (num_valor > 1800) and (num_valor < 2100):
                # obten la última cifra. Esto nos servirá para identificar el tipo de redondeo a aplicar
                ultima_cifra = num_valor % 10

                '''
				Para redondear, usaremos ceil (que redondea al alza) si el número termina en 5 o mas,
				o floor (que redondea por debajo) si el número termina en menos que 5
				'''
                if(ultima_cifra >= 5):
                    redondeo = math.ceil(num_valor/10)*10
                else:
                    redondeo = math.floor(num_valor/10)*10
            else:
                return Resultado.ERROR_NO_ANIO

            # reemplazamos el valor del campo por su redondeo
            campo_reg.text = str(redondeo)
        else:
            return Resultado.ERROR_NO_NUMERO

        return Resultado.CORRECTO
