import pandas as pd

import variables_conf as var_conf


class GeneraTablas:

    '''
    Borra los espacios en las cabeceras de las columnas.

    Recibe por parámetro el dataframe de la tabla correspondiente
    Devuelve el dataframe modificado sin espacios en los nombres de columnas
    '''

    def _borrar_espacios_columnas(self, df_tabla: pd.DataFrame()):
        # recorre las columnas para borrar los espacios de las cabeceras
        for columna in df_tabla:

            # si el nombre de la columna tiene espacios
            if " " in columna:
                # borra los espacios del nombre de la columna
                nombre_sin_espacios = columna.replace(" ", "")

                # cambia el nombre de la columna por el nombre sin espacios
                df_tabla.rename(
                    columns={columna: nombre_sin_espacios}, inplace=True)

        return df_tabla

    '''
    Genera una colección de ficheros csv a partir de las hojas del fichero xls (Excel) proporcionado
    Cada hoja se exportará en forma de un fichero csv, con el nombre de la misma
    '''

    def generar_csv_desde_xls(self, fichero_xls: str):

        # obten el directorio donde vamos a guardar los ficheros generados
        directorio = var_conf.RUTA_FICH_DESCRIPTOR

        # obten las hojas del fichero excel
        xls = pd.ExcelFile(fichero_xls)
        hojas_excel = xls.sheet_names

        # recorre las hojas del fichero excel, y exportalas como csv
        for hoja in hojas_excel:
            df_hoja = xls.parse(hoja)

            # borra los espacios de las cabeceras en las columnas
            df_sin_espacios = self._borrar_espacios_columnas(df_hoja)

            # exporta el dataframe a csv
            nombre_csv = f"{directorio}/{hoja}.csv"
            df_sin_espacios.to_csv(nombre_csv, sep=";", index=False)


if __name__ == "__main__":
    genera_tablas = GeneraTablas()

    genera_tablas.generar_csv_desde_xls("auxiliares/siiu_Codigos_RRHH.xls")
    genera_tablas.generar_csv_desde_xls("auxiliares/siiu_Codigos_auxiliar.xls")
    genera_tablas.generar_csv_desde_xls("auxiliares/siiu_Codigos_becas.xls")
    genera_tablas.generar_csv_desde_xls(
        "auxiliares/siiu_Codigos_area académica.xls")
