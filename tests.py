from genera_tablas import GeneraTablas
import variables_conf as var_conf
import glob


import pytest


def test_generacsv():
    genera_tablas = GeneraTablas()
    genera_tablas.generar_csv_desde_xls(
        f"{var_conf.RUTA_FICH_DESCRIPTOR}/siiu_Codigos_auxiliar.xls")

    lista_ficheros = ["MASTER_TABLA.csv", "InterUniversitario.csv", "Tipo_de_Unidad.csv", "Situación_Estudios.csv", "Situación.csv", "Tipos_de_Otras_Unidades.csv",
                      "Tipo_de_Centro.csv", "Rama_de_Enseñanza.csv", "Presencialidad_del_Estudio.csv", "PCEO_TABLA_.csv", "Universidad.csv", "TODAS_UNIDADES_POR_CURSO.csv"]

    ruta_aux = f"{var_conf.RUTA_FICH_DESCRIPTOR}"

    lista_existe = []

    for fichero in lista_ficheros:
        ruta_fichero = f"{ruta_aux}/{fichero}"

        if glob.glob(ruta_fichero):
            lista_existe.append(fichero)

    assert lista_ficheros == lista_existe
